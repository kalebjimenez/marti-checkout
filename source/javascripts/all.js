$(document).ready(function(){
  function toggle(title, body) {
    $(title).on('click',function(){
      $(body).toggle('slow');
      $('.flow-buttons').toggle();
    });
  }
  toggle('.add-item__header','.add-item__body');

  /* tooltipster */

  $('.tooltip').tooltipster();


  /* disable form submit */

  $('form').submit(function(e) {
    e.preventDefault();
  });


  /* Change copy according to flow */

  function changeCopy(el) {
    if(el.hasClass('checkout-user')) {
      el.find('#register').find('.steps-header').find('h2').text(' Carlos Rodriguez').prepend('<i class="fa fa-user-circle" aria-hidden="true"></i>');
    } else {
      el.find('#register').find('.steps-header').find('h2').prepend('<i class="fa fa-user-circle" aria-hidden="true"></i>');
    }
    if(el.hasClass('checkout-guest')) {
      el.find('#register').find('.steps-header').find('h2').text(' Compra invitado').prepend('<i class="fa fa-user-circle" aria-hidden="true"></i>');
    }
  }

  changeCopy($('main'))


function nextStep(form) {
  var currentContent = $(form).closest('.steps-content');
  var nextContent = currentContent.closest('section').next('section').find('.steps-content');

  var currentSectionHeader = currentContent.prev();
  var nextSectionHeader = nextContent.prev();

  var currentSection = currentContent.closest('section');
  var nextSection = currentSection.next('section');

  currentSectionHeader.removeClass('current-step').addClass('prev-step');
  nextSectionHeader.removeClass('next-step').addClass('current-step');

  currentSection.removeClass('current-section');
  nextSection.addClass('current-section');

  currentContent.slideUp();
  nextContent.slideDown();

  if (nextSection.hasClass('last-section')) {
    nextSection.closest('.steps').addClass('full-width');
    $('.details').hide();
    $('.prev-step').closest('section').hide();
  }

  // add default option for payment
  if (nextSection.is($('#payment'))) {
    if($('.options-item-cards')) {
      var options = $('.options-item-cards');
      var option = $(options[0]);
      option.addClass('selected');
      option.find('input[type="radio"]').click()
    }
  }

}


  /* form validation */

  $('form').validate({
    submitHandler: function(form) {
     return false;
    }
  });

  $('.btn-save').on('click',function(){
    var form = $(this).closest('form').valid();
    if (form === true) {
      nextStep(this);
    }
  });

  /* funtion alert */

  function ok(){
  	alertify.success("Notificación correcta");
  	return false;
  }


  /* hide shipping form */

  $('input[name="same-address"]').on('click',function(){
    var checked = $(this).is(':checked');
    if (checked) {
      $('.shipping-form-pay').hide();
    } else {
      $('.shipping-form-pay').show();
    }
  })


  function toggleOptionItemForm() {
    $('.shipping-form-wrapper').toggle("slow");
    $('.add-button-wrapper').toggle("slow");
    $('.button-wrapper').toggle();
    $('.options').toggle();
  };

  function cancelNewOptionItem() {
    $('.shipping-form-wrapper').toggle("slow");
    $('.add-button-wrapper').toggle();
    $('.button-wrapper').toggle();
    $('.options').toggle();
  }

  $('.btn-add').on('click',function(){
    toggleOptionItemForm();
  });

  $('.btn-cancel-option').on('click',function(){
    cancelNewOptionItem();
  });


    /* Toggle item selector */

    function optionItemFeedback(input) {
      $('.form-radio input[type="radio"]').closest('.options-item').removeClass('selected');
      input.closest('.options-item').addClass('selected');
    }

    $('.form-radio input[type="radio"]').on('click',function(){
      optionItemFeedback($(this));
    });

    //add checked attr to default address

    if ($('.options-item-address')) {
      var options = $('.options-item-address');
      options.each(function(){
        if($(this).hasClass('selected')) {
          $(this).find('input[type="radio"]').removeAttr('checked').attr('checked','checked').click();
        }
      });
    }
});
